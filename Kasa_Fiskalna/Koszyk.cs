﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KasaFiskalna
{
    static class Koszyk
    {
        private static List<Produkt> zakupy = new List<Produkt>();

        public static List<Produkt> Zakupy { get { return zakupy; } }

        public static void Nowy()
        {
            zakupy.Clear();
        }
    }
}
