﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KasaFiskalna
{
    class Produkt : ICloneable
    {
        private string nazwa;
        private double cenaJednostkowa;
        private uint ilosc;

        public string Nazwa { get { return nazwa; } }
        public double CenaJednostkowa { get { return cenaJednostkowa; } }
        public uint Ilosc { get { return ilosc; } }

        public Produkt(string nazwa, double cenaJednostkowa, uint ilosc)
        {
            this.nazwa = nazwa;
            this.cenaJednostkowa = cenaJednostkowa;
            this.ilosc = ilosc;
        }

        public object Clone()
        {
            return new Produkt(nazwa, cenaJednostkowa, ilosc);
        }
    }
}
