﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KasaFiskalna
{
    static class Aplikacja
    {
        private static ConsoleKey klawisz = ConsoleKey.NoName;
        public static bool Koniec = false;

        public static void WczytajKlawisz()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Dzień dobry!");
            Console.ResetColor();
            Console.WriteLine("Co chcesz zrobic? Wcisnij odpowiedni klawisz.");

            Console.WriteLine("Legenda:");
            Console.WriteLine("\tP - dodaj produkt do koszyka");
            Console.WriteLine("\tK - skopiuj ostatnio wprowadzony produkt");
            Console.WriteLine("\tZ - pokaz zawartosc koszyka");
            /*Console.WriteLine("\tS - pokaz sume do zaplaty");
            Console.WriteLine("\tX - skasuj produkt z listy");
            Console.WriteLine("\tW - wydrukuj paragon");*/
            Console.WriteLine("\tN - dodaj nowy koszyk");
            Console.WriteLine("\tE - zakoncz dzialanie programu");
            Console.WriteLine();

            Console.Write("Wybór: ");
            klawisz = Console.ReadKey().Key;
        }

        public static void WykonajDzialanie()
        {
            Console.Clear();

            switch (klawisz)
            {
                case ConsoleKey.P: DodajProdukt(); break;
                case ConsoleKey.K: KopiujProdukt(); break;
                case ConsoleKey.Z: PokazKoszyk(); break;
                case ConsoleKey.S: break;
                case ConsoleKey.X: break;
                case ConsoleKey.W: break;
                case ConsoleKey.N: Koszyk.Nowy(); break;
                case ConsoleKey.Escape: case ConsoleKey.E: Koniec = true; break;
                default: Koniec = false; break;
            }
        }

        private static void KopiujProdukt()
        {
            if (Koszyk.Zakupy.Count < 1)
            {
                Console.WriteLine("Najpierw dodaj jakiś produkt.");
                Console.ReadKey();
                return;
            }

            Produkt kopia = Koszyk.Zakupy[Koszyk.Zakupy.Count - 1].Clone() as Produkt;
            if (kopia != null)
            {
                Koszyk.Zakupy.Add(kopia);
                Console.WriteLine("Skopiowano produkt.");

            }
            else
            {
                Console.WriteLine("Nie udalo sie skopiowac produktu.");
            }
            Console.ReadKey();
        }

        private static void PokazKoszyk()
        {
            Console.WriteLine("Zawartosc koszyka");
            if (Koszyk.Zakupy.Count < 1)
                Console.WriteLine("<pusty>");
            else
            {
                foreach (Produkt p in Koszyk.Zakupy)
                {
                    Console.WriteLine("{0}x {1} ({2} PLN)", p.Ilosc, p.Nazwa, p.CenaJednostkowa);
                }
            }
            
            Console.ReadKey();
        }

        private static void DodajProdukt()
        {
            string nazwa;
            double cena;
            uint ilosc;

            Console.WriteLine("Dodawanie produktu");

            Console.Write("Wprowadź nazwe: ");
            nazwa = Console.ReadLine();

            Console.Write("Wprowadź cene: ");
            cena = Convert.ToDouble(Console.ReadLine());

            Console.Write("Wprowadź ilosc: ");
            ilosc = Convert.ToUInt32(Console.ReadLine());

            Koszyk.Zakupy.Add(new Produkt(nazwa, cena, ilosc));
        }
    }
}
